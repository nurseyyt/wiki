# **Skyblock Guide 2.0**

## Content

*   [Island](#Island)
    *   [Settings](#Settings)
    *   [Member](#Member)
    *   [Bank](#Bank)
    *   [Value](#Value)
    *   [Warps](#warps)
*   [Missions](#Missions)
    *   [Weekly](#Weekly Missions)
    *   [Island](#Island Missions)
    *   [Achievements](#Achievements)
*   [PVE Mines](#PVE Mines)
    *   [Coal](#Coal)
    *   [Iron](#Iron)
    *   [Gold](#Gold)
    *   [Diamond](#Diamond)
    *   [Emerald](#Emerald)
    *   [Netherite](#Netherite)
*   [Events](#Events)
    *   [Koth](#Koth)
    *   [Envoy](#Envoy)
*   [Cluescrolls](#Cluescrolls)
    *   [Common](#Common)
    *   [Rare](#Rare)
    *   [Legendary](#Legendary)
    *   [Mythic](#Mythic)
*   [Pets](#Pets)
    *   [Common](#Common Pets)
    *   [Uncommon](#Uncommon Pets)
    *   [Rare](#Rare Pets)
    *   [Legendary](#Legendary Pets)
    *   [Mythic](#Mythic Pets)
    *   [Pet Leash](#Pet Leash)
    *   [Rare Candy](#Rare Candy)
*   [Void/Chunk Chest](#Void/Chunk Chest)
## Island

### Settings
### Member
### Bank
### Value
### Warps

## Missions

### Weekly Missions
### Island Missions
### Achievements

## PVE Mines

### Coal
### Iron
### Gold
### Diamond
### Emerald
### Netherite

## Events

### Koth
### Envoy

## Cluescrolls

### Common
### Rare
### Legendary
### Mythic

## Pets

### Common Pets
### Uncommen Pets
### Rare Pets
### Legendary Pets
### Mythic Pets
### Pet Leash
### Rare Candy

## Void/Chunk Chest

# Dropdowns
<details closed>
<summary>Want to ruin the surprise?</summary>
<br>
Well, you asked for it!
</details>
